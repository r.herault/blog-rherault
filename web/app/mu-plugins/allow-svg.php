<?php
/**
 * Plugin Name:  Allow SVG
 * Description:  Add filter to allow to use svg to media library
 * Version:      1.0.0
 * Author:       Romain Herault
 * Author URI:   https://rherault.fr
 * License:      MIT License
 */

function wp_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'wp_mime_types');
<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;700&family=Roboto:wght@100;400;700&display=swap" rel="preload" as="style">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;700&family=Roboto:wght@100;400;700&display=swap" rel="stylesheet" media="print" onload="this.media='all'">

    <noscript>
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;700&family=Roboto:wght@100;400;700&display=swap" rel="stylesheet">
    </noscript>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'susty'); ?></a>

	<header id="nav" class="fixed top-0 z-30 w-full px-8 mt-4">
        <div id="nav-button" class="flex flex-col items-end justify-center w-12 h-12 ml-auto navbar">
            <span></span>
            <span></span>
        </div>

        <nav id="main-nav" class="main-nav" role="navigation">
            <ul class="main-nav__list">
                <li class="main-nav__item">
                    <a href="https://rherault.fr" class="main-nav__link">Accueil</a>
                </li>

                <li class="main-nav__item">
                    <a href="https://rherault.fr/a-propos" class="main-nav__link">À propos</a>
                </li>

                <li class="main-nav__item">
                    <a href="https://rherault.fr/projets" class="main-nav__link">Projets</a>
                </li>

                <li class="main-nav__item">
                    <a href="<?= get_home_url() ?>" class="main-nav__link">Blog</a>
                </li>

                <li class="main-nav__item">
                    <a href="https://rherault.fr/contact" class="main-nav__link">Contact</a>
                </li>
            </ul>
        </nav>
    </header>

	<div id="content">

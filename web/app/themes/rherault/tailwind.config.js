module.exports = {
  purge: [
      './**/*.php',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
          primary: "#f95959",
          beige: "#e3e3e3"
        }
      },
      fontFamily: {
        display: ["Raleway", "sans-serif"],
        body: ["Roboto", "sans-serif"]
      },

      container: {
        center: true,
        padding: "1rem"
      }
  },
  variants: {
    extend: {},
  },
  plugins: [
      require('@tailwindcss/forms')
  ],
}

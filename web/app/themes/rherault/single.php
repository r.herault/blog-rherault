<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

	<div id="primary">
		<?php
        while (have_posts()) :
            the_post();

            get_template_part('template-parts/content', get_post_type());

            //the_post_navigation();
            ?>
            <div class="container">
                <div class="px-4 py-6 mb-6 flex flex-col md:flex-row justify-between space-y-4 md:space-y-0">
                    <div class="site__navigation">
                        <?php previous_post_link('Article Précédent<br>%link'); ?>
                    </div>
                    <div class="site__navigation md:text-right">
                        <?php next_post_link('Article Suivant<br>%link'); ?>
                    </div>
                </div>
            </div>

            <?php

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) : ?>
            <?php
                comments_template();
            endif;

        endwhile; // End of the loop.
        ?>


	</div><!-- #primary -->

<?php
get_footer();

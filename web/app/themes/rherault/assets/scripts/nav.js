document.addEventListener('DOMContentLoaded', function() {
	let nav = document.getElementsByClassName('navbar');

	Array.from(nav).forEach(elem => {
		elem.addEventListener('click', () => {
            document.getElementById('nav-button').classList.toggle('navbar-open');
            document.getElementById('main-nav').classList.toggle('active');
		});
    });
});

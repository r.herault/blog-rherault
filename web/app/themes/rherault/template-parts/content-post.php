<?php
/**
 * Template part for displaying single post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="flex flex-col items-center justify-center bg-primary h-mid-full">
        <h1 class="text-6xl text-center text-white mb-4"><?= get_the_title() ?></h1>
        <span class="flex items-center text-white">
            <svg class="w-5 mr-2" fill="currentColor" viewbox="0 0 20 20">
                <path d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd" fill-rule="evenodd"></path>
            </svg>
            <?php susty_wp_posted_on() ?>
        </span>
    </header>

	<?php // susty_wp_post_thumbnail();?>

	<main class="container -mt-20">
        <div class="px-4 py-6 mb-6 bg-white rounded shadow-lg content">
            <?php
            the_content();

            wp_link_pages(array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'susty'),
                'after'  => '</div>',
            ));
            ?>
        </div>
	</main>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('text-center'); ?>>
	<header>
		<?php
        if (is_singular()) :
            the_title('<h1>', '</h1>');
        else :
            the_title('<h2 class="font-medium"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        endif;

        if ('post' === get_post_type()) :
            ?>
            <div class="flex items-center justify-center space-x-2 divide-x article-meta">
						<span class="flex items-center text-primary">
							<svg class="w-5 mr-2" fill="currentColor" viewbox="0 0 20 20">
								<path d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd" fill-rule="evenodd"></path>
							</svg>
                            <?php susty_wp_posted_on() ?>
						</span>
                        <div class="flex items-center px-2 meta-tags">
                            <svg class="w-5 mr-2" fill="currentColor" viewbox="0 0 20 20">
                                <path d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" fill-rule="evenodd"></path>
                            </svg>
                            <span><?php susty_wp_categories() ?></span>
						</div>
					</div>
		<?php endif; ?>
	</header>

	<?php // susty_wp_post_thumbnail();?>

	<div class="text-gray-700">
		<?php
        the_excerpt();

        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'susty'),
            'after'  => '</div>',
        ));
        ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

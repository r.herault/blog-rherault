<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

get_header();
?>

	<div id="primary">
		<div id="main">

		<?php
        if (have_posts()) :?>
				<header class="flex items-center justify-center bg-primary h-mid-full">
					<h1 class="text-6xl text-white">Blog</h1>
				</header>

                <main class="container -mt-24">
                    <div class="px-4 py-6 space-y-4 bg-white divide-y rounded shadow-lg">
				<?php
            /* Start the Loop */
            while (have_posts()) :
                the_post();

                /*
                 * Include the Post-Type-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                 */
                get_template_part('template-parts/content');

            endwhile;

            the_posts_navigation();

        else:

            get_template_part('template-parts/content', 'none');

        endif;
        ?>

                </div>
            </main>
		</div>
	</div>

<?php
get_footer();
